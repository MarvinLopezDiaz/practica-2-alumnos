const express = require('express');
const router = express.Router();
const conexion = require('../database/db');
const crud = require('../controllers/crud');

router.get('/',(req,res)=>{
    conexion.query('SELECT * FROM alumno', (error, resultados)=>{
        if(error){
            throw error;
        }else{
            res.render('index', {results:resultados});
        }
    });
})

router.get('/views/createAlumno',(req,res)=>{
    res.render('createAlumno');

});

router.get('/views/editarAlumno/:matricula', (req,res)=>{
    const matricula = req.params.matricula;
    conexion.query('SELECT * FROM alumno WHERE matricula=?',[matricula], (error,resultados)=>{
        if(error){
            throw error;
        }else{
            res.render('editarAlumno', {alumno:resultados[0]});
        }
    })
});

router.get('/delete/:id', (req,res)=>{
    const id = req.params.id;
    conexion.query('DELETE FROM alumno WHERE id = ?',[id], (error,resultados)=>{
        if(error){
            throw error;
        }else{
            res.redirect('/');
        }
    })
});

router.post('/save', crud.save);

router.post('/update', crud.update);
module.exports = router;
