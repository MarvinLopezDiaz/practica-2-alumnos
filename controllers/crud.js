const conexion = require('../database/db');

exports.save = ((req, res)=>{
    const nombre = req.body.nombre;
    const matricula = req.body.matricula;
    const domicilio = req.body.domicilio;
    const sexo = req.body.sexo;
    const especialidad = req.body.especialidad;
    conexion.query('INSERT INTO alumno SET ?', {nombre, matricula, domicilio, sexo, especialidad}, (error, resultados)=>{
        if(error){
            console.log(error);
        }else{
            res.redirect('/');
        }
    })
});

exports.update = ((req, res)=>{
    const id = req.body.id;
    const nombre = req.body.nombre;
    const matricula = req.body.matricula;
    const domicilio = req.body.domicilio;
    const sexo = req.body.sexo;
    const especialidad = req.body.especialidad;
    conexion.query('UPDATE alumno SET ? WHERE matricula = ?', [{nombre:nombre, matricula:matricula, domicilio:domicilio, sexo:sexo, especialidad:especialidad}, matricula], (error, resultados)=>{
        if(error){
            console.log(error);
        }else{
            res.redirect('/');
        }
    })
});